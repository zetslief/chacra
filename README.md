# chacra

The attempt is to create a platform for online gaming:
* First game is basic tennis.
* The platform should run on a public server, available online.

## TODO

* `lobby/host`: use `REST` for data.
* `lobby` should have unique `id`, not name.
* Fix lobby creation: `playerName` is used at the moment, which is _sub-optimal_.